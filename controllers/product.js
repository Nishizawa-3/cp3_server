const Product = require("../models/product")
const Order = require("../models/order")


//For posting a new product
module.exports.postProduct = (body) => {

	let newProduct = new Product({
		name: body.name,
		productCategory: body.productCategory,
		description: body.description,
		price: body.price,
		stock: body.stock
	})

	return newProduct.save().then((product,error) =>{

		if(error){
			return false; 
		}else {
			return true; 
		}
	})
}

//For retrieving all the available products
module.exports.getAllProducts =()=>{
	return Product.find().then(result =>{
		return result
	})
}

//For getting a specific product
module.exports.getProduct = (params)=>{
	// findById is a Mongoose operation that just finds a document by its ID
	return Product.findById(params.productId).then(result =>{
		return result
	})
}


//For updating a specific product
module.exports.updateProduct = (params, body) => {
	let updatedProduct = {
		name:body.name,
		productCategory:body.productCategory,
		description: body.description,
		price: body.price,
		isAvailable: body.isAvailable,
		stock: body.stock
	}

	return Product.findByIdAndUpdate(params.productId, updatedProduct).then((product,err) =>{
		if(err){
			return false
		}else{
			return true
		}
	})
}


//For archiving/deleting a product
module.exports.archiveProduct = (params) =>{
	let archivedProduct = {
		isAvailable: false,
		stock: 0
	}

	return Product.findByIdAndUpdate(params.productId, archivedProduct).then((product, err)=>{
		if(err){
			return false
		}else{
			return true
		}
	})
}


// module.exports.writeReview = (params, reviewer)=>{
// 	return db.orders.find({userId:reviewer.userId}).then(result =>{

// 		if(result.length == 0){
// 			return false
// 		}else{
// 			product.reviews.push({reviews: reviewer.body})
// 			return product.save().then((review,err)=>{
// 				if(err){
// 						return false
// 					}else{
// 						return true
// 					}
// 			})
// 		}
// 	})
// }








