const express = require ("express")
const mongoose = require ("mongoose")

const userRoutes = require("./routes/user")
const productRoutes = require("./routes/product")
const orderRoutes = require("./routes/order")
const cors = require("cors")


// add database connection
mongoose.connect("mongodb+srv://admin:admin@cluster0.aq4ae.mongodb.net/b126_online_shopping?retryWrites=true&w=majority",{
	useNewUrlParser: true,
	useUnifiedTopology: true
})

mongoose.connection.once('open', () => console.log("Now connected to MongoDB Atlas"))

const app = express()

const corsOptions = {
	origin: 'http://localhost:3000',
	optionsSuccessStatus: 200
}

app.use(cors(corsOptions))
app.use(express.json())
app.use(express.urlencoded({
	extended: true
}))

app.use("/users", userRoutes)
app.use("/products", productRoutes)
app.use("/orders", orderRoutes)

const port = 4000

app.listen(process.env.PORT || port, () =>{
	console.log(`Server running at port ${port}`)
})

























