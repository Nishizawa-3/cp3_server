const express = require('express')
const router = express.Router()
const productController = require("../controllers/product")
const auth = require("../auth")


//Route for posting a new product
router.post("/", auth.verify, (req, res)=>{
	
	if (auth.decode(req.headers.authorization).isAdmin){
		productController.postProduct(req.body).then(resultFromController => res.send(resultFromController))
	}else{
		return res.send({auth: "failed"})
	}
})


//Route for retrieving all available products
router.get("/", (req, res)=> {
	productController.getAllProducts().then(resultFromController => res.send(resultFromController))
})


//Route for getting a specific product
router.get("/:productId", (req, res) => {
	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController))
})


//Route for updating existing product info
router.put("/:productId", auth.verify, (req, res) =>{
	if (auth.decode(req.headers.authorization).isAdmin){
		productController.updateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController))
	}else{
		return res.send({auth: "failed"})
	}
})


//Route for deleting/archiving a product
router.delete("/:productId", auth.verify, (req, res)=>{
	if (auth.decode(req.headers.authorization).isAdmin){
		productController.archiveProduct(req.params).then(resultFromController => res.send(resultFromController))
	}else{
		return res.send({auth: "failed"})
	}
})

// router.post("/:productId", auth.verify, (req, res) =>{

// 	let reviewer = {
// 		userId: auth.decode(req.headers.authorization).id,
// 		review: req.body.review,
// 	}

// 	if(auth.decode(req.headers.authorization).isAdmin){
// 		return res.send("You are not allowed to post a review")
// 	}else{
// 		productController.writeReview(req.params, req.reviewer).then(resultFromController => res.send(resultFromController))
// 	}
// })

module.exports = router