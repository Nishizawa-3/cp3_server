const mongoose = require("mongoose")

const userSchema = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, "First name is required"]
	},
	lastName: {
		type: String,
		required: [true, "Last name is required"]
	},
	email:{
		type: String,
		required: [true, "Email is required"]
	},
	password:{
		type: String,
		required: [true, "Password name is required"]
	},
	isAdmin:{
		type: Boolean,
		default: false
	},
	mobileNo:{
		type: String,
		required: [true, "Mobile number is required"]
	},
	address:{
			street:{
				type: String,
				required: [true, "street is required"]
			},
			city:{
				type: String,
				required: [true, "city is required"]
			},
			state:{
				type: String,
				required: [true, "state is required"]
			},
			zipCode: String,	
	}
})

module.exports = mongoose.model("User", userSchema)
